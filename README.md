API-PONTO-ELETRONICO
END-POINTs
--> USUARIO (localhost:8080/usuario)
--> REGISTRO DE PONTO (localhost:8080/registroponto)
GET:        /usuario - Consulta todos os usuários cadastrados
/usuario/{idUsuario} - Consulta dados de apenas um usuário
POST:       /usuario - Cadastra um novo usuário (Informar json no body)
PUT:        /usuario/{idUsuario} - Atualiza os dados do usuário informado (Informar json no body)
POST:       /registroponto?idUsuario - Inclui novo registro de ponto para o usuário (Informar json no body)
GET:        /registroponto?idUsuario - Lista informações do usuário, lista de registro de pontos e total de horas acumulado.


----------------------------------------

GET - localhost:8080/usuario
POST - localhost:8080/usuario
PUT - localhost:8080/usuario/1
{
    "nomeCompleto": "XXXx",
    "cpf":"9999999",
    "email": "email@email.com"
}



----------------------------------------

GET - localhost:8080/registroponto?idUsuario=1
---> resposta:
{
    "usuario": {
        "id": 1,
        "nomeCompleto": "XXXX",
        "cpf": "123123",
        "email": "email@email.com",
        "dataCadastro": "2020-07-06T18:40:19"
    },
    "registrosPonto": {
        "2020-07-06T21:32:38": "ENTRADA",
        "2020-07-06T21:32:50": "SAIDA",
        "2020-07-06T21:33:14": "ENTRADA",
        "2020-07-06T21:33:38": "SAIDA",
        "2020-07-06T21:34:23": "ENTRADA",
        "2020-07-06T21:34:32": "SAIDA",
        "2020-07-06T21:34:51": "ENTRADA",
        "2020-07-06T21:35:49": "SAIDA",
        "2020-07-06T21:36:09": "ENTRADA",
        "2020-07-06T21:37:55": "SAIDA"
    },
    "horasTrabalhadas": "00:03:29"
}


POST - localhost:8080/registroponto?idUsuario=1
--> Entrada
{
    "tipoRegistroPonto":"SAIDA"
}

--> Saída
{
    "id": 28,
    "tipoRegistroPonto": "SAIDA",
    "usuario": {
        "id": 2,
        "nomeCompleto": "XXX",
        "cpf": "123123",
        "email": "email@email.com",
        "dataCadastro": "2020-07-06T18:37:06"
    },
    "dataHoraBatida": "2020-07-06T21:40:54.531"
}
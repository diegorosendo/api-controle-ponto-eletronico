package br.com.itau.pontoeletronico.pontoeletronico.services;

import br.com.itau.pontoeletronico.enums.TipoRegistroPontoEnum;
import br.com.itau.pontoeletronico.models.DTOs.RegistroPontoDTO;
import br.com.itau.pontoeletronico.models.RegistroPonto;
import br.com.itau.pontoeletronico.models.Usuario;
import br.com.itau.pontoeletronico.repositories.RegistroPontoRepository;
import br.com.itau.pontoeletronico.repositories.UsuarioRepository;
import br.com.itau.pontoeletronico.services.RegistroPontoService;
import br.com.itau.pontoeletronico.services.UsuarioService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class RegistroPontoServiceTest {

    @Autowired
    private RegistroPontoService registroPontoService;

    @MockBean
    private RegistroPontoRepository registroPontoRepository;

    @MockBean
    private UsuarioRepository usuarioRepository;

    @MockBean
    private UsuarioService usuarioService;


    Usuario usuario;
    RegistroPonto registroPonto;

    @BeforeEach
    public void setUp() {
        usuario = new Usuario();
        usuario.setId(1);
        usuario.setCpf("29734148818");
        usuario.setNomeCompleto("Diego Rosendo");
        usuario.setEmail("diego@email.com");
        usuario.setDataCadastro(LocalDateTime.now());
        Optional<Usuario> usuarioOptional = Optional.of(usuario);
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(usuarioOptional);
    }

    @Test
    public void testarRegistrarPontoDeEntradaOK() {
        List<RegistroPonto> registroPontos = new ArrayList<>();

        Mockito.when(registroPontoRepository.findAllByUsuario(Mockito.any(Usuario.class))).thenReturn(registroPontos);
        Mockito.when(usuarioService.consultarUsuario(Mockito.anyInt())).thenReturn(usuario);

        registroPonto = new RegistroPonto();
        registroPonto.setTipoRegistroPonto(TipoRegistroPontoEnum.ENTRADA);
        registroPonto.setDataHoraRegistro(LocalDateTime.now());
        registroPonto.setUsuario(usuario);

        Mockito.when(registroPontoRepository.save(Mockito.any(RegistroPonto.class))).thenReturn(registroPonto);

        RegistroPonto pontoObjeto = registroPontoService.registrarPonto(1, registroPonto);

        Assertions.assertEquals(pontoObjeto.getTipoRegistroPonto(), TipoRegistroPontoEnum.ENTRADA);
        Assertions.assertEquals(pontoObjeto.getUsuario(), usuario);
    }

    @Test
    public void testarRegistrarPontoDeSaidaAntesDaEntradaNegativo() {
        List<RegistroPonto> pontos = new ArrayList<>();
        Mockito.when(registroPontoRepository.findAllByUsuario(Mockito.any(Usuario.class))).thenReturn(pontos);
        Mockito.when(registroPontoRepository.save(Mockito.any(RegistroPonto.class))).thenReturn(registroPonto);

        Assertions.assertThrows(RuntimeException.class, () -> {
            registroPontoService.registrarPonto(1, registroPonto);
        });
    }




}


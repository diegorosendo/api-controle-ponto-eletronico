package br.com.itau.pontoeletronico.pontoeletronico.services;

import br.com.itau.pontoeletronico.models.Usuario;
import br.com.itau.pontoeletronico.repositories.UsuarioRepository;
import br.com.itau.pontoeletronico.services.UsuarioService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Optional;

@SpringBootTest
public class UsuarioServiceTest {

    @Autowired
    private UsuarioService usuarioService;

    @MockBean
    private UsuarioRepository usuarioRepository;

    Usuario usuario;

    @BeforeEach
    public void setUp() {
        usuario = new Usuario();
        usuario.setId(1);
        usuario.setCpf("29734148818");
        usuario.setNomeCompleto("Diego Rosendo");
        usuario.setEmail("diego@emai.com");
        usuario.setDataCadastro(LocalDateTime.now());

    }

    @Test
    public void criarUmNovoUsuarioOK() {
        Mockito.when(usuarioRepository.save(Mockito.any(Usuario.class))).thenReturn(usuario);
        Usuario usuarioObjeto = usuarioService.criarUsuario(usuario);

        Assertions.assertEquals(usuarioObjeto.getId(), 1);
        Assertions.assertEquals(usuarioObjeto.getEmail(), usuario.getEmail());

    }

    @Test
    public void testarEditarUsuarioOK() {
        Optional<Usuario> usuarioOptional = Optional.of(usuario);
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(usuarioOptional);
        Mockito.when(usuarioRepository.save(Mockito.any(Usuario.class))).thenReturn(usuario);

        usuario.setNomeCompleto("Adriana");
        usuario.setEmail("adriana@email.com");
        usuario.setCpf("29734148819");

        Usuario usuarioObjeto = usuarioService.editarUsuario(1, usuario);

        Assertions.assertEquals(usuario, usuarioObjeto);
    }

    @Test
    public void testarAtualizarUmUsuarioQueNaoExisteNegativo() {
        Optional<Usuario> usuarioOptional = Optional.empty();
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(usuarioOptional);
        usuario.setNomeCompleto("Adriana");
        usuario.setEmail("adriana@email.com");
        usuario.setCpf("29734148818");

        Assertions.assertThrows(RuntimeException.class, () -> {
            usuarioService.editarUsuario(999, usuario);
        });
    }

    @Test
    public void testarConsultarUmUsuarioOK() {
        Optional<Usuario> usuarioOptional = Optional.of(usuario);
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(usuarioOptional);
        Usuario usuarioObjeto = usuarioService.consultarUsuario(1);

        Assertions.assertEquals(usuario, usuarioObjeto);
    }

    @Test
    public void testarConsultarUmUsuarioQueNaoExisteNegativo() {
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenThrow(RuntimeException.class);
        Assertions.assertThrows(RuntimeException.class, () -> {
            usuarioService.consultarUsuario(2);
        });
    }

    @Test
    public void testarConsultarTodosUsuariosOK() {
        Iterable<Usuario> usuarios = Arrays.asList(usuario);
        Mockito.when(usuarioRepository.findAll()).thenReturn(usuarios);
        Iterable<Usuario> usuarioIterable = usuarioService.listarTodosOsUsuarios();
        Assertions.assertEquals(usuarios, usuarioIterable);
    }
}

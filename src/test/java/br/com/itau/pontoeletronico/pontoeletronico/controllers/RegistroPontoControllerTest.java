package br.com.itau.pontoeletronico.pontoeletronico.controllers;

import br.com.itau.pontoeletronico.controllers.RegistroPontoController;
import br.com.itau.pontoeletronico.enums.TipoRegistroPontoEnum;
import br.com.itau.pontoeletronico.models.DTOs.RegistroPontoDTO;
import br.com.itau.pontoeletronico.models.RegistroPonto;
import br.com.itau.pontoeletronico.models.Usuario;
import br.com.itau.pontoeletronico.services.RegistroPontoService;
import br.com.itau.pontoeletronico.services.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.TreeMap;

@WebMvcTest(RegistroPontoController.class)
public class RegistroPontoControllerTest {


    @MockBean
    private UsuarioService usuarioService;

    @MockBean
    private RegistroPontoService registroPontoService;

    @Autowired
    private MockMvc mockMvc;

    Usuario usuario;
    RegistroPonto registroPonto;

    @BeforeEach
    public void setUp() {
        usuario = new Usuario();
        usuario.setNomeCompleto("Diego Rosendo");
        usuario.setCpf("29734148818");
        usuario.setEmail("diego@email.com");
        registroPonto = new RegistroPonto();
    }


    @Test
    public void consultarPontosRegistradosOK() throws Exception {
        Mockito.when(registroPontoService.consultarPontosRegistrados(Mockito.anyInt()))
                .then(pontoObjeto -> {
                    RegistroPontoDTO pontoSaidaDTO = new RegistroPontoDTO();
                    registroPonto.setId(1);
                    registroPonto.setUsuario(usuario);
                    registroPonto.setDataHoraRegistro(LocalDateTime.now());
                    registroPonto.setTipoRegistroPonto(TipoRegistroPontoEnum.ENTRADA);
                    usuario.setId(1);
                    usuario.setDataCadastro(LocalDateTime.now());
                    pontoSaidaDTO.setUsuario(usuario);
                    TreeMap<LocalDateTime, TipoRegistroPontoEnum> registrosPonto = new TreeMap<>();
                    registrosPonto.put(registroPonto.getDataHoraBatida(), registroPonto.getTipoRegistroPonto());
                    pontoSaidaDTO.setRegistrosPonto(registrosPonto);
                    pontoSaidaDTO.setHorasTrabalhadas(LocalTime.of(1, 0));
                    return pontoSaidaDTO;
                });

        ObjectMapper mapper = new ObjectMapper();
        String jsonDePonto = mapper.writeValueAsString(registroPonto);

        mockMvc.perform(MockMvcRequestBuilders.get("/registroponto?idUsuario=1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDePonto))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.usuario.id", CoreMatchers.equalTo(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.registrosPonto",
                        CoreMatchers.hasItems()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.horasTrabalhadas",
                        CoreMatchers.notNullValue()));
    }



}

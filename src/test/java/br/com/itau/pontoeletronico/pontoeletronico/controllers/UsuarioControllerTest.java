package br.com.itau.pontoeletronico.pontoeletronico.controllers;

import br.com.itau.pontoeletronico.controllers.UsuarioController;
import br.com.itau.pontoeletronico.enums.TipoRegistroPontoEnum;
import br.com.itau.pontoeletronico.models.RegistroPonto;
import br.com.itau.pontoeletronico.models.Usuario;
import br.com.itau.pontoeletronico.models.DTOs.RegistroPontoDTO;
import br.com.itau.pontoeletronico.services.RegistroPontoService;
import br.com.itau.pontoeletronico.services.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.TreeMap;

@WebMvcTest(UsuarioController.class)
public class UsuarioControllerTest {

    @MockBean
    private UsuarioService usuarioService;

    @MockBean
    private RegistroPontoService registroPontoService;

    @Autowired
    private MockMvc mockMvc;

    Usuario usuario;
    RegistroPonto registroPonto;

    @BeforeEach
    public void setUp() {
        usuario = new Usuario();
        usuario.setNomeCompleto("Diego Rosendo");
        usuario.setCpf("29734148818");
        usuario.setEmail("diego@email.com");
        registroPonto = new RegistroPonto();
    }

    @Test
    public void testarCriarUsuarioOK() throws Exception {
        Mockito.when(usuarioService.criarUsuario(Mockito.any(Usuario.class)))
                .then(usuarioObjeto -> {
                    usuario.setId(1);
                    usuario.setDataCadastro(LocalDateTime.now());
                    return usuario;
                });

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeUsuario = mapper.writeValueAsString(usuario);

        mockMvc.perform(MockMvcRequestBuilders.post("/usuario")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
        }

}


package br.com.itau.pontoeletronico.models.DTOs;

import br.com.itau.pontoeletronico.enums.TipoRegistroPontoEnum;
import br.com.itau.pontoeletronico.models.Usuario;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

public class RegistroPontoDTO {


    private Usuario usuario;
    private TreeMap<LocalDateTime, TipoRegistroPontoEnum> registrosPonto;
    private LocalTime horasTrabalhadas;

    public RegistroPontoDTO() {
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public TreeMap<LocalDateTime, TipoRegistroPontoEnum> getRegistrosPonto() {
        return registrosPonto;
    }

    public void setRegistrosPonto(TreeMap<LocalDateTime, TipoRegistroPontoEnum> registrosPonto) {
        this.registrosPonto = registrosPonto;
    }

    public LocalTime getHorasTrabalhadas() {
        return horasTrabalhadas;
    }

    public void setHorasTrabalhadas(LocalTime horasTrabalhadas) {
        this.horasTrabalhadas = horasTrabalhadas;
    }

    public void calcularHorasTrabalhadas() {
        List<LocalDateTime> localDateTimes = new ArrayList(registrosPonto.keySet());
        List<LocalTime> localTimes = new ArrayList<>();
        for(LocalDateTime l : localDateTimes){
            localTimes.add(l.toLocalTime());
        }

        if (registrosPonto.isEmpty()) {
            setHorasTrabalhadas(LocalTime.parse("00:00:00.00"));
        } else {

            LocalTime horasUltimoPeriodo;

            if (localDateTimes.size() % 2 != 0) {
                localTimes.add(LocalTime.now());
            }

            LocalTime horasPeriodoSaidaMenosEntrada;
            LocalTime horasTotais = LocalTime.parse("00:00:00.00");
            for (int i=localTimes.size()-1; i > 0; i -= 2) {
                horasPeriodoSaidaMenosEntrada = localTimes.get(i).minusNanos(localTimes.get(i-1).toNanoOfDay());
                horasTotais = horasTotais
                        .plusHours(horasPeriodoSaidaMenosEntrada.getHour())
                        .plusMinutes(horasPeriodoSaidaMenosEntrada.getMinute())
                        .plusSeconds(horasPeriodoSaidaMenosEntrada.getSecond())
                        .plusNanos(horasPeriodoSaidaMenosEntrada.getNano());
            }

            setHorasTrabalhadas(horasTotais);

        }

    }


}



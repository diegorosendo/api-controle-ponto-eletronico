package br.com.itau.pontoeletronico.models;

import br.com.itau.pontoeletronico.enums.TipoRegistroPontoEnum;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

@Entity
@Table(name = "registro_ponto")
public class RegistroPonto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @DateTimeFormat
    private LocalDateTime dataHoraRegistro;

    @NotNull
    private TipoRegistroPontoEnum tipoRegistroPonto;

    @ManyToOne(cascade = CascadeType.ALL)
    private Usuario usuario;

    public RegistroPonto() {
    }

    public RegistroPonto(Usuario usuario, TipoRegistroPontoEnum tipoBatidaPonto) {
        this.usuario = usuario;
        this.tipoRegistroPonto = tipoBatidaPonto;
        this.dataHoraRegistro = LocalDateTime.now();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public LocalDateTime getDataHoraBatida() {
        return dataHoraRegistro;
    }

    public void setDataHoraRegistro(LocalDateTime dataHoraRegistro) {
        this.dataHoraRegistro = dataHoraRegistro;
    }

    public TipoRegistroPontoEnum getTipoRegistroPonto() {
        return tipoRegistroPonto;
    }

    public void setTipoRegistroPonto(TipoRegistroPontoEnum tipoRegistroPonto) {
        this.tipoRegistroPonto = tipoRegistroPonto;
    }

}
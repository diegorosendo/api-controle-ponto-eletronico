package br.com.itau.pontoeletronico.services;

import br.com.itau.pontoeletronico.models.Usuario;
import br.com.itau.pontoeletronico.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    public Usuario criarUsuario(Usuario usuario) {
        usuario.setDataCadastro(LocalDateTime.now());
        Usuario usuarioObjeto = usuarioRepository.save(usuario);
        return usuarioObjeto;
    }

    public Usuario editarUsuario(int id, Usuario usuario ) {
        Optional<Usuario> usuarioOptional = usuarioRepository.findById(id);
        if (usuarioOptional.isPresent()) {
            usuario.setId(usuarioOptional.get().getId());
            usuario.setDataCadastro(usuarioOptional.get().getDataCadastro());
            Usuario usuarioObjeto = usuarioRepository.save(usuario);
            return usuarioObjeto;
        }
        throw new RuntimeException("Usuário não localizado");
    }

    public Usuario consultarUsuario(int id) {
        Optional<Usuario> usuarioOptional = usuarioRepository.findById(id);
        if (usuarioOptional.isPresent()) {
            return usuarioOptional.get();
        }
        throw new RuntimeException("Usuário não localizado");
    }

    public Iterable<Usuario> listarTodosOsUsuarios() {
        Iterable<Usuario> usuarioIterable = usuarioRepository.findAll();
        return usuarioIterable;
    }

}

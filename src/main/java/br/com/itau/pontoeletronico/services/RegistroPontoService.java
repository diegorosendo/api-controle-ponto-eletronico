package br.com.itau.pontoeletronico.services;

import br.com.itau.pontoeletronico.enums.TipoRegistroPontoEnum;
import br.com.itau.pontoeletronico.models.DTOs.RegistroPontoDTO;
import br.com.itau.pontoeletronico.models.RegistroPonto;
import br.com.itau.pontoeletronico.models.Usuario;
import br.com.itau.pontoeletronico.repositories.RegistroPontoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.TreeMap;

@Service
public class RegistroPontoService {

    @Autowired
    private RegistroPontoRepository registroPontoRepository;

    @Autowired
    UsuarioService usuarioService;


    public RegistroPonto registrarPonto(int id, RegistroPonto registroPonto) {
        Usuario usuario = usuarioService.consultarUsuario(id);
        registroPonto.setUsuario(usuario);
        registroPonto.setDataHoraRegistro(LocalDateTime.now());

        if (this.lancamentoValido (registroPonto)) {
            RegistroPonto registroPontoObjeto = registroPontoRepository.save(registroPonto);
            return registroPontoObjeto;
        }

        if (registroPonto.getTipoRegistroPonto() == TipoRegistroPontoEnum.ENTRADA) {
            throw new RuntimeException("Opção ENTRADA inválida. Registre a SAIDA");
        }
        if (registroPonto.getTipoRegistroPonto() == TipoRegistroPontoEnum.SAIDA) {
            throw new RuntimeException("Opção SAIDA inválida. Registre a ENTRADA");
        }

        throw new RuntimeException("Exceção não prevista");

    }

    private boolean lancamentoValido (RegistroPonto registroPonto){
        RegistroPonto ultimoRegistroPonto = registroPontoRepository.findTopByUsuarioOrderByDataHoraRegistroDesc(registroPonto.getUsuario());
        if (ultimoRegistroPonto == null){
            if (registroPonto.getTipoRegistroPonto() == TipoRegistroPontoEnum.ENTRADA) {
                return true;
            }
            else{
                return false;
            }
        }else {
            if (ultimoRegistroPonto.getTipoRegistroPonto() != registroPonto.getTipoRegistroPonto())
            {
                return true;
            }
            else{
                return false;
            }

        }

    }

    public RegistroPontoDTO consultarPontosRegistrados(int id) {
        Usuario usuario = usuarioService.consultarUsuario(id);
        Iterable<RegistroPonto> registrosPonto = registroPontoRepository.findAllByUsuario(usuario);
        TreeMap<LocalDateTime, TipoRegistroPontoEnum> registroPontoList = new TreeMap<>();
        for (RegistroPonto p : registrosPonto) {
            registroPontoList.put(p.getDataHoraBatida(), p.getTipoRegistroPonto());
        }
        RegistroPontoDTO registroPontoDTO = new RegistroPontoDTO();
        registroPontoDTO.setUsuario(usuario);
        registroPontoDTO.setRegistrosPonto(registroPontoList);
        registroPontoDTO.calcularHorasTrabalhadas();
        return registroPontoDTO;
    }


}

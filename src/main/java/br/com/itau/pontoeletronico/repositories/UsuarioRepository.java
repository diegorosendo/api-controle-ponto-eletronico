package br.com.itau.pontoeletronico.repositories;

import br.com.itau.pontoeletronico.models.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository <Usuario, Integer> {
}

package br.com.itau.pontoeletronico.repositories;

import br.com.itau.pontoeletronico.models.RegistroPonto;
import br.com.itau.pontoeletronico.models.Usuario;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RegistroPontoRepository extends CrudRepository <RegistroPonto, Integer> {
    List<RegistroPonto> findAllByUsuario (Usuario usuario);

    RegistroPonto findTopByUsuarioOrderByDataHoraRegistroDesc(Usuario usuario);

}

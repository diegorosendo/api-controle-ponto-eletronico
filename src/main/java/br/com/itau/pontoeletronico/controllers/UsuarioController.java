package br.com.itau.pontoeletronico.controllers;


import br.com.itau.pontoeletronico.enums.TipoRegistroPontoEnum;
import br.com.itau.pontoeletronico.models.DTOs.RegistroPontoDTO;
import br.com.itau.pontoeletronico.models.RegistroPonto;
import br.com.itau.pontoeletronico.models.Usuario;
import br.com.itau.pontoeletronico.services.RegistroPontoService;
import br.com.itau.pontoeletronico.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;
    @Autowired
    private RegistroPontoService registroPontoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Usuario criarUsuario(@RequestBody @Valid Usuario usuario) {
        Usuario usuarioObjeto = usuarioService.criarUsuario(usuario);
        return usuarioObjeto;
    }

    @PutMapping("/{id}")
    public Usuario editarUsuario(@PathVariable int id, @RequestBody @Valid Usuario usuario) {
        try {
            Usuario usuarioObjeto = usuarioService.editarUsuario(id, usuario);
            return usuarioObjeto;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }


    @GetMapping
    public Iterable<Usuario> consultarTodosUsuarios() {
        Iterable<Usuario> usuario = usuarioService.listarTodosOsUsuarios();
        return usuario;
    }

}

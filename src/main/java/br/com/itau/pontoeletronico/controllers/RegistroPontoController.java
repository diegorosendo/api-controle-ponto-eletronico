package br.com.itau.pontoeletronico.controllers;

import br.com.itau.pontoeletronico.enums.TipoRegistroPontoEnum;
import br.com.itau.pontoeletronico.models.DTOs.RegistroPontoDTO;
import br.com.itau.pontoeletronico.models.RegistroPonto;
import br.com.itau.pontoeletronico.services.RegistroPontoService;
import br.com.itau.pontoeletronico.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/registroponto")
public class RegistroPontoController {

    @Autowired
    private RegistroPontoService registroPontoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public RegistroPonto registrarPonto(@RequestParam(name = "idUsuario", required = true) int idUsuario, @RequestBody @Valid RegistroPonto registroPonto) {
        try {
            RegistroPonto registroPontoObjeto = registroPontoService.registrarPonto(idUsuario, registroPonto);
            return registroPontoObjeto;
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }



    @GetMapping
    public RegistroPontoDTO consultarPontosRegistrados (@RequestParam(name = "idUsuario", required = false) Optional<Integer> idUsuarioOptional){

        if (idUsuarioOptional.isPresent())
        {
            int idUsuario = idUsuarioOptional.get();
            try {
                RegistroPontoDTO registroPontoDTO = registroPontoService.consultarPontosRegistrados(idUsuario);
                return registroPontoDTO;
            } catch (RuntimeException e){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
            }

        }

        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Informe um idUsuario");

    }


}

package br.com.itau.pontoeletronico.enums;

public enum TipoRegistroPontoEnum {
        ENTRADA,
        SAIDA
}
